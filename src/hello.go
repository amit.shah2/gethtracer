package main

import (
	"fmt"
	"math/rand"
	"github.com/json-iterator/go"
	"os/signal"
	"sync"
	"sync/atomic"
	"syscall"
	"time"
	"io/ioutil"
	"net/http"
	"os"
	"bytes"

)

// how long in seconds events will be collected prior to writing
const batchTime = 3
var json = jsoniter.ConfigCompatibleWithStandardLibrary

func init() {

	// always seed the random
	rand.Seed(time.Now().UnixNano())

}

type RPCPayload struct {
	Jsonrpc string `json:"jsonrpc"`
	Method  string `json:"method"`
	Params  `json:"params"`
	ID      int `json:"id"`
}
type Params []interface{}

// RunContext which combines all the channels
type RunContext struct {
	inChan    chan Event
	batchChan chan EventBatch
	backChan  chan EventBatch
	doneChan  chan bool

	// used to report results at the end
	sumProduced int64
	sumSent     int64

}

// NewRunContext build a new run context which holds
// all channels used in this pipeline
func NewRunContext() *RunContext {
	return &RunContext{
		inChan:    make(chan Event, 10),
		//batchChan: make(chan EventBatch),
		//backChan:  make(chan EventBatch, 1),
		doneChan:  make(chan bool),
	}
}

// Event simple event
type Event int64

// EventBatch which is just a slice of Event
type EventBatch []Event

// NewEventBatch make a new event empty batch
func NewEventBatch() EventBatch { return EventBatch{} }

// Merge given an event append it to the event batch
func (e EventBatch) Merge(other Event) EventBatch { 
	return append(e, other) 
}

func traceBlock(blockNum int) ([]byte, error) {
	payloadBytes, err := json.Marshal(RPCPayload{"2.0", "debug_traceBlockByNumber", Params{fmt.Sprintf("0x%x", blockNum)}, 2})
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	body := bytes.NewReader(payloadBytes)
	req, err := http.NewRequest("POST", "http://127.0.0.1:8888", body)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	tracesBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	defer resp.Body.Close()
	return tracesBody, nil
}


func produce(done<- struct{} , blockNumberChannel <- int, addressChannel -> byte[], wg *sync.WaitGroup){
	
	defer  runCtx.wg.Done()
	for {
		select {
		case blockNum,_ := <- blockNumberChannel:
			addressChannel <- traceBlock(blockNum)
			//atomic.AddInt64(&runCtx.sumProduced, int64(e)) // build a sum of the events produced
		case <-done:
			fmt.Println("producer complete")
			return
		}
	}
	
}

func run(runCtx *RunContext, wg *sync.WaitGroup) {
	defer wg.Done()

	eventbatch := NewEventBatch()

	// Collect events for the configured batch time
	// this needs to be tuned based on how often you want to
	// flush data to the writer
	ticker := time.Tick(time.Duration(batchTime) * time.Second)

LOOP:
	for {
		select {
		case ev, ok := <-runCtx.inChan:
			if !ok {
				if len(eventbatch) > 0 {
					fmt.Println("Dispatching last batch")
					runCtx.batchChan <- eventbatch
				}
				close(runCtx.batchChan)
				fmt.Println("run finished")
				break LOOP
			}

			eventbatch = eventbatch.Merge(ev)

		case <-ticker:

			if len(eventbatch) > 0 {
				fmt.Println("Waiting to send")
				runCtx.batchChan <- eventbatch
				eventbatch = <-runCtx.backChan
			}
		}
	}
}

func batchWriter(runCtx *RunContext, wg *sync.WaitGroup) {
	defer wg.Done()

	for {
		eb, ok := <-runCtx.batchChan
		if !ok {
			fmt.Println("Batch writer complete")
			break
		}

		// simulate time to persist the batch using a random delay
		delay := time.Duration(rand.Intn(3)+1) * time.Second
		time.Sleep(delay)

		// We will need to retry if this write fails
		// and add a exponential backoff

		for _, e := range eb {
			atomic.AddInt64(&runCtx.sumSent, int64(e))
		}
		fmt.Println("Batch sent:", eb, delay)

		runCtx.backChan <- NewEventBatch()
	}

}

func waitOnSignal(done<- struct{}, sigs <-chan os.Signal) {
	fmt.Println("awaiting signal")
	defer close(done)	//broadcasts to all done listners in
	sig := <-sigs
	fmt.Println(sig)
}

const N_RPCProcess = 5

func main() {

	var wg sync.WaitGroup

	runCtx := NewRunContext()

	doneChan:  make(chan bool)

	blockNumberChannel := make(chan int )
	defer close(blockChannel)

	addressChannel := make(chan []byte N_WriteProcess)
	defer close(addressChannel)




	sigs := make(chan os.Signal, 1)
	defer close(sigs)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go waitOnSignal(done, sigs)


	wg.Add(N_RPCProcess)
	for i := 0; i < N_RPCProcess; i++ {
		go produce(blockNumberChannel, addressChannel, &wg)
	}
	// if you hit CTRL-C or kill the process this channel will
	// get a signal and trigger a shutdown of the publisher
	// which in turn should trigger a each step of the pipeline
	// to exit
	
	// wg.Add(1)
	// go produce(runCtx, &wg)
	// wg.Add(2)
	// go run(runCtx, &wg)
	// go batchWriter(runCtx, &wg)
	for bn:= Start_Block; block < End_Block; bn++{
		select{
			blockChannel <- bn
	}
	wg.Wait()

	fmt.Print("\nSummary\n")
	fmt.Printf(" produced: %d\n", runCtx.sumProduced)
	fmt.Printf("     sent: %d\n", runCtx.sumSent)
	panic("SHUTTING DOWN")
}
