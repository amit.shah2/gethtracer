package types

type RPCPayload struct {
	Jsonrpc string `json:"jsonrpc"`
	Method  string `json:"method"`
	Params  `json:"params"`
	ID      int `json:"id"`
}

type Params []interface{}

type TraceRPCResult struct {
	Jsonrpc string `json:"jsonrpc"`
	Result  []TraceResult `json:"result"`
	Id int `json:"id"`
}


type TraceResult struct{
	Result struct{
		Op string `json:"type"`
		To string `json:"to"`
		From string `json:"from"`
		Value string `json:"value"`
		Gas string `json:"gas"`
		GasUsed string `json:"gasUsed"`
		Input string `json:"input"`
		Output string `json:"output"`
		Error string `json:"error"`
		Time string `json:"time"`
		Calls []TraceResult `json:"calls"`
		Store []StoreResult `json:"store"`
		Log []LogResult `json:"log"`
	} `json:"result"`
	
} 

type StoreResult struct{
	Op string `json:"op"`
	Address string `json:"address"`
	Key string `json:"key"`
	NewValue string `json:"newValue"`
	OldValue string `json:"oldValue"`
}

type LogResult struct{
	Op string `json:"op"`
	Params string `json:"params"`
	Topics []string `json:"topics"`

}