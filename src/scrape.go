package main

// The aim of this example is to illustrate backpressure using golang channels and go routines.
//
// This is the basis for a simple data processing service which could either be reading from
// some internal queue or a socket of some sort.

import (
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"sync"
	"sync/atomic"
	"syscall"
	"time"
	"github.com/json-iterator/go"
	"io/ioutil"
	"net/http"
	"bytes"
	"rmcmt"
	"types"
)

// how long in seconds events will be collected prior to writing
const batchTime = 3
var json = jsoniter.ConfigCompatibleWithStandardLibrary
const N_Producers = 1
const Start_Block = 0
const End_Block= 6000000
var Tracer_JS = string(loadCallTracer("/Users/amitshah/Documents/projects/BreadWallet/GoTrace/resources/call_tracer.js"))




func loadCallTracer(srcpath string) ([]byte){
	b, err := ioutil.ReadFile(srcpath)
	if err != nil {
		panic(err)
	}
	return rmcmt.RemoveCppStyleComments(rmcmt.RemoveCStyleComments(b))
}

func traceBlock(blockNum int) ([]byte, error) {
	payloadBytes, err := json.Marshal(types.RPCPayload{"2.0", "debug_traceBlockByNumber", types.Params{fmt.Sprintf("0x%x", blockNum),  struct{Id string `json:"tracer"`} { Tracer_JS }}, 2})
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	body := bytes.NewReader(payloadBytes)
	req, err := http.NewRequest("POST", "http://127.0.0.1:8888", body)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	tracesBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	defer resp.Body.Close()
	return tracesBody, nil
}

func init() {

	// always seed the random
	rand.Seed(time.Now().UnixNano())

}

// RunContext which combines all the channels
type RunContext struct {
	wg sync.WaitGroup
	
	traceChannel    chan int
	metadataChannel chan []byte
	writeChannel chan []byte
	batchChan chan EventBatch
	backChan  chan EventBatch
	doneChan  chan bool
	
	// used to report results at the end
	sumProduced int64
	sumSent     int64
}

// NewRunContext build a new run context which holds
// all channels used in this pipeline
func NewRunContext() *RunContext {
	return &RunContext{
		wg : sync.WaitGroup{},
		
		traceChannel:    make(chan int, N_Producers),
		//metadataChannel: make(chan []byte),
		writeChannel: 	make(chan []byte, N_Producers),
		//inChan:    make(chan Event),
		//batchChan: make(chan EventBatch),
		//backChan:  make(chan EventBatch, 1),
		doneChan:  make(chan bool),

	}
}

// Event simple event
type Event int64

// EventBatch which is just a slice of Event
type EventBatch []Event

// NewEventBatch make a new event empty batch
func NewEventBatch() EventBatch { return EventBatch{} }

// Merge given an event append it to the event batch
func (e EventBatch) Merge(other Event) EventBatch { return append(e, other) }

//https://www.opsdash.com/blog/job-queues-in-go.html
//consider the priorty before sending job down to write channel
//after a goroutine process's a job, it goes back into the select loop
//there is no "fairness" and it may not pickup the doneChan signal and instead process
//of the buffered traceChannel and go into deadlock if it tried to writeChannel
//we thus unblocked the writeChannel and allow it to continue to process the items off the queue
//until it is exhausted.  
func produce(runCtx *RunContext,n int) {
	
	defer runCtx.wg.Done()
	fmt.Println("producer started:",n)
LOOP:
	for {

		// simulate some delay between groups of incoming events
		select {
			case blockNum, ok:= <-runCtx.traceChannel:
				if(!ok){
					break LOOP
				}
				if  trace,err := traceBlock(blockNum); trace!=nil {
					//fmt.Println(string(trace))
					runCtx.writeChannel<- trace 					
				}else{
					fmt.Println("trace for block %s failed", err)
				}
			//any go routines that havent started new work are told to exit immediately on sigTerm
			case <-runCtx.doneChan:
				fmt.Println("producer complete:", n)

				return
		}
		
	}
}


func run(runCtx *RunContext) {
	//run has to consume everything off the writeChannel and cannot simply exit on done signal
	//or else our consumers will block on the writeChannel.  We cancel from top down and let the processing complete

	//traceBatch := NewEventBatch()

	// Collect events for the configured batch time
	// this needs to be tuned based on how often you want to
	// flush data to the writer
	ticker := time.Tick(time.Duration(batchTime) * time.Second)

LOOP:
	for {
		select {
		case trace, ok := <-runCtx.writeChannel:
			if !ok {
				// if len(eventbatch) > 0 {
				// 	fmt.Println("Dispatching last batch")
				// 	runCtx.batchChan <- eventbatch
				// }
				// close(runCtx.batchChan)
				// fmt.Println("run finished")
				break LOOP
			}
			//fmt.Println("consume trace")
			fmt.Println(string(trace))
			var traces types.TraceRPCResult
			err := json.Unmarshal(trace, &traces)
			if err != nil {
				fmt.Println(err)				
			}
			fmt.Print(traces.Result)

			//eventbatch = eventbatch.Merge(ev)

		case <-ticker:

			//if len(eventbatch) > 0 {
				fmt.Println("Waiting to send")
				//runCtx.batchChan <- eventbatch
				//eventbatch = <-runCtx.backChan
			//}
		
		}
		
	}
}

// func exractValueTransfer(traceBytes []byte){
// 	var traces types.TraceResult
	

// }

func generator(runCtx *RunContext, Start_Block, End_Block int){
	defer runCtx.wg.Done()
	defer close(runCtx.traceChannel)
	for blockNum:= Start_Block; blockNum < End_Block; blockNum++{
		select {
			case runCtx.traceChannel<- blockNum:
				fmt.Println("sending block %d", blockNum)
			case <-runCtx.doneChan:
				fmt.Println("generator complete")
				return
			}
		}
}

func batchWriter(runCtx *RunContext) {
	defer runCtx.wg.Done()

	for {
		eb, ok := <-runCtx.batchChan
		if !ok {
			fmt.Println("Batch writer complete")
			break
		}

		// simulate time to persist the batch using a random delay
		delay := time.Duration(rand.Intn(3)+1) * time.Second
		time.Sleep(delay)

		// We will need to retry if this write fails
		// and add a exponential backoff

		for _, e := range eb {
			atomic.AddInt64(&runCtx.sumSent, int64(e))
		}
		fmt.Println("Batch sent:", eb, delay)

		runCtx.backChan <- NewEventBatch()
	}

}

func waitOnSignal(runCtx *RunContext, sigs <-chan os.Signal) {
	defer close(runCtx.doneChan)
	fmt.Println("awaiting signal")
	sig := <-sigs
	fmt.Println(sig)
	// shut down input
	
}

	
func main() {
	//var wg sync.WaitGroup
	

	runCtx := NewRunContext()

	sigs := make(chan os.Signal, 1)

	// if you hit CTRL-C or kill the process this channel will
	// get a signal and trigger a shutdown of the publisher
	// which in turn should trigger a each step of the pipeline
	// to exit
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go waitOnSignal(runCtx, sigs)


	runCtx.wg.Add(N_Producers)
	for i := 0; i < N_Producers; i++ {
		go produce(runCtx,i)
	}

	go run(runCtx)

	// go batchWriter(runCtx, &wg)

	// go func(){ 
	// 	for blockNum:= Start_Block; blockNum < End_Block; blockNum++{
	// 		select	{
	// 				case runCtx.traceChannel<-blockNum:
	// 					fmt.Println("sending block %d", blockNum)
	// 				case <-runCtx.doneChan:
	// 					fmt.Println("producer complete")
	// 					return
	// 			}
	// 	}
	// 	defer close(runCtx.traceChannel)
	// 	defer close(runCtx.writeChannel)
	// }()//for the defer
	runCtx.wg.Add(1)
	go generator(runCtx, Start_Block,End_Block)
	//trace,_:= traceBlock(End_Block);
 	//fmt.Println(string(trace));
	//runCtx.wgProducers.Wait()
	runCtx.wg.Wait()
	defer close(runCtx.writeChannel)///because we have multiple routines writing to this channel, we cannot close it form inside there
	
	fmt.Print("\nSummary\n")
	fmt.Printf(" produced: %d\n", runCtx.sumProduced)
	fmt.Printf("     sent: %d\n", runCtx.sumSent)
}